<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Typw" content="text/html; charset=UTF-8">
		<title>簡易Twitter</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div>
			<div class="header">
				<c:if test="${empty loginUser }">
					<a href="login">ログイン</a>
					<a href="signup">登録する</a>
				</c:if>
				<c:if test="${not empty loginUser }">
					<a href="./">ホーム</a>
					<a href="setting">設定</a>
					<a href="logout">ログアウト</a>
				</c:if>
				<c:if test="${not empty loginUser }">
					<div class="profile">
						<div class="name"><h2><c:out value="${loginUser.name }"></c:out></h2></div>
						<div class="account">@<c:out value="${loginUser.account }"></c:out></div>
						<div class="description"><c:out value="${loginUser.description }"></c:out></div>
					</div>
				</c:if>
			</div>
		</div>
		<c:if test="${not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages }" var="errorMessage">
						<li><c:out value="${errorMessage }"/>
					</c:forEach>
				</ul>
			</div>
		</c:if>

		<div class="form-area">
			<c:if test="${isShowMessageForm }">
				<form action="message" method="post">
					いまどうしてる？<br>
					<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
					<br>
					<input type="submit" value="つぶやく">(140文字まで)
				</form>
			</c:if>
		</div>
		<div>
			<c:forEach items="${messages }" var="message">
				<div class="message">
					<div class="account-name">
						<span class="account">
							<a href="./?user_id=<c:out value="${message.userId }"/>">
								<c:out value="${message.account }"/>
							</a>
						</span>
						<span class="name"><c:out value="${message.created_date }"/></span>
					</div>
					<div class="text"><c:out value="${message.text }"/></div>
					<div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
				</div>
			</c:forEach>
		</div>

		<div class="copyright">Copyright(c) Ashiri Tominaga</div>
	</body>
</html>